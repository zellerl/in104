#ifndef INC_POD_H
#define INC_POD_H

#include <SFML/System.hpp>
#include <cmath>

const int NB_PODS = 3;
const int R_CP = 10; //rayon CP à vérifier plus tard

struct GameInfos;

struct Decision
{
    Decision(sf::Vector2f target, float power, int laser);

    sf::Vector2f target_;
    float power_;
    int laser_; //renvoie l'information de si le pod tire ou non du laser
};

class Pod
{
    public :
    Pod(sf::Vector2f pos, float angle, sf::Vector2f vel = {0, 0});
    Decision getDecision(GameInfos gameInfos) const; 

    private :
    sf::Vector2f pos_, vel_; //position and velocity vectors
    float angle_; //angle in radians
    int nextCP_, lapCount_;
    
    int AI_;
    int laser_; //0 = pas de laser / 1 =  tir un laser
    int nb_lasers_;//nb e tour restant ou il peut irer du laser
    friend class Game; //allows game to modify pod's private attributes
};

#endif