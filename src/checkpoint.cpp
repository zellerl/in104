#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include "checkpoint.h"
#include "game.h"


const float RADIUS_CP = 5; 

CheckPoint::CheckPoint(sf::Vector2f center, unsigned int id)
{
    circle_ = sf::CircleShape (RADIUS_CP);
    font_.loadFromFile("/home/ensta/Documents/IN104/repository/Fredoka-Bold.ttf");
    fillingText_.setString(std::to_string(id)); 
    circle_.setPosition(center.x, center.y);

    
};

void CheckPoint::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(circle_, states);
    target.draw(fillingText_);
};

sf::Vector2f CheckPoint::getPosition()
{
    return circle_.getPosition();
}

FinalCheckPoint::FinalCheckPoint(sf::Vector2f center)
{
    circle_ = sf::CircleShape (RADIUS_CP);
    // si pas beau, ajouter un setorigintocentershape...
    
    fillingTexture_.loadFromFile("/home/ensta/Documents/IN104/repository/Images/checkeredFlag.png") ;
    fillingSprite_.setTexture(fillingTexture_);
    setOriginToCenter(fillingSprite_);
    scaleByRadius(fillingSprite_, 550);
    fillingSprite_.setPosition(center);
}

void FinalCheckPoint::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(circle_, states);
    target.draw(fillingSprite_, states);
};

sf::Vector2f FinalCheckPoint::getPosition()
{
    return circle_.getPosition();
}