#include "pod.h"
#include "game.h"
#include <SFML/System/Vector2.hpp>
#include <cstdio>

Decision::Decision(sf::Vector2f target, float power, int laser) : target_(target), power_(power), laser_(laser)
{
    target_ = target ;
    power_ = power ;
    laser_ = laser;
};

Pod::Pod(sf::Vector2f pos, float angle, sf::Vector2f vel) 
{
    pos_ = pos;
    vel_ = vel;
    angle_= angle;
    nextCP_= 1;    //init du prochain cp a viser
    lapCount_ = 0; //init a 0 tour

    AI_ = 1;
    laser_ = 0;
    nb_lasers_ = 15;

};

Decision Pod::getDecision(GameInfos gameInfos) const
{

    float powertest = 100;
    sf::Vector2f targettest = gameInfos.cpPositions[nextCP_]; //souvent source de plantage
    int laser = 0;

    if (AI_ == 0){
        targettest = pos_;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
        // la touche "flèche gauche" est enfoncée : on bouge le personnage
            targettest.x -= 10;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
       
            targettest.x += 10;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
       
            targettest.y -= 10;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
        
            targettest.x += 10;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {//la touche espace est enfoncée : pour ce tour, le pod tire un laser
            laser = 1;

        }
    }

    return Decision(targettest, powertest, laser);
};
;