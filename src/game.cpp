#include "game.h"
#include "checkpoint.h"
#include "pod.h"
#include "utils.h"
#include <SFML/System/Vector2.hpp>
#include <cmath>
#include <SFML/Graphics/CircleShape.hpp>
#include <cstdlib>
#include <iostream>



Game::Game(std::vector<sf::Vector2f> checkpointsPositions) : finalCP_(checkpointsPositions[0])
{

     std :: vector <sf::Vector2f> cpPositions_ = checkpointsPositions;

    backgroundTexture_.loadFromFile("../repository/Images/background.png");

    sf::Sprite backgroundSprite_;
    backgroundSprite_.setTexture(backgroundTexture_);
    setOriginToCenter(backgroundSprite_);

    scaleToMinSize(backgroundSprite_,H_SCREEN,W_SCREEN);
    //backgroundSprite_.setPosition(H_SCREEN/.2,W_SCREEN/.2);
    

    for (int i = 0; i<NB_CPS; i++)
    {
        otherCPs_.emplace_back(checkpointsPositions[i], i);  
    }

   
}


void Game::addPod()
{
    podsSprites_.reserve(NB_PODS);
    pods_.reserve(NB_PODS);
    podsTextures_.reserve(NB_PODS);
    podsLasers.reserve(NB_PODS);
 
    Pod pod_1 = Pod({11000,8000}, 0.f);  
    sf::Texture pod_Texture1;
    sf::Sprite pod_Sprite1;
    pod_1.AI_ = 0;

    if (!pod_Texture1.loadFromFile("/home/ensta/IN104/repository/Images/BSGCylon.png"))
    {
        printf("Erreur fichier\n");
    }
    
    podsTextures_.emplace_back(pod_Texture1);
    pods_.emplace_back(pod_1);
    podsSprites_.emplace_back(pod_Sprite1);

       
    Pod pod2 = Pod({11000,8000}, 0.f);
    sf::Texture podTexture2;
    sf::Sprite podSprite2;

    if (!podTexture2.loadFromFile("/home/ensta/IN104/repository/Images/BSGViper.png"))
    {
        printf("Erreur fichier\n");
    }

    podsTextures_.emplace_back(podTexture2);
    pods_.emplace_back(pod2);
    podsSprites_.emplace_back(podSprite2); 
    

    for (int i=0; i<NB_PODS; i++){
        
        podsSprites_[i].setTexture(podsTextures_[i]);
        setOriginToCenter(podsSprites_[i]);
        scaleToMaxSize(podsSprites_[i], 800,800);

    }
 
}


void Game::updatePhysics()
{

    for (Pod &pod : pods_)
    {
    //on stocke momentanement la position du pod pour pourvoir faire l'interpolation linéaire entre ses 2 positions
    float x_p = pod.pos_.x;
    float y_p = pod.pos_.y;


    Decision d = pod.getDecision({pods_, cpPositions_});
    float m = 0.5;

    pod.laser_ = d.laser_;
    if (d.laser_){
    pod.nb_lasers_ -= 1;
    }

    float norm = pow (pow((d.target_.x- pod.pos_.x), 2) + pow((d.target_.x- pod.pos_.x), 2), 0.5);
    pod.vel_ =  m * ( pod.vel_ + d.power_*(d.target_-pod.pos_)/norm) ;
    pod.pos_ = pod.pos_ + pod.vel_;
    // actuelllement le pod peut sortir de l'écran, ce n'est pas un problème


    float x_c = cpPositions_[pod.nextCP_].x;
    float y_c = cpPositions_[pod.nextCP_].y;

    // vérification du passage ou non par un check-point :
    // on calcule l'intersection entre le cercle du checkpoint et la trajectoire du vaisseau
    // on va avoir à résoudre l'équation de degrès 2 de coefficiants a, b, c
    float a = pow((x_p - pod.pos_.x),2) + pow((y_p - pod.pos_.y),2);
    float b = 2*((x_p - pod.pos_.x)*(pod.pos_.x - x_c) + (y_p - pod.pos_.y)*(pod.pos_.y - y_c));
    float c = pow((pod.pos_.x - x_c), 2) + pow((pod.pos_.y - y_c), 2) - pow(R_CP, 2);

    float delta = pow(b,2) - 4*a*c;
    float t_1 = (b/2 - sqrt(delta))/(2*a);
    float t_2 = (b/2 + sqrt(delta))/(2*a);

    if (t_1 <= 1 || t_2 >=0){ //c'est à dire qu'il y a un point du segment entre les deux position dans le cercle
        pod.nextCP_ = pod.nextCP_ + 1;
    }

    //vérification qu'il n'y a pas d'intersection avec un laser :
    for (int j = 0; j<= NB_PODS; j++)
    {
        if (pods_[j].pos_ != pod.pos_)
        {
            if (pods_[j].laser_ && pods_[j].nb_lasers_) 
            {
            //calcul de la distance entre le rayon laser (même diection que pod.vel) et le pod attaqué
            float npVel =  pow((pow(pod.vel_.x, 2) + pow(pod.vel_.y, 2)), 0.5);
            sf::Vector2f pVel_n = pod.vel_ /npVel;

            sf::Vector2f dif_pos = pods_[j].pos_ - pod.pos_;
            float ndPos = pow (pow(dif_pos.x, 2) + pow(dif_pos.y, 2), 0.5);
            sf::Vector2f difPos_n = dif_pos / ndPos;

            sf::Vector2f dif_vel = pVel_n - difPos_n;
            float ndVel = pow (pow(dif_vel.x, 2) + pow(dif_vel.y, 2), 0.5);

            if (ndVel < 0.2) 
            {
                d.power_ = 0;
            }

            }
        }
    }

    }

    physicsTime += PHYSICS_TIME_STEP;

    


    
    
}

void Game::updateGraphics(sf::Time frameTime)
{
    //utiliser la position de la podSprite (ancienne position graphique) et la position du pod
    //(prochaine position réelle), physicsTime_, lastFrameTime_ et frameTime pour évaluer la nouvelle position graphique
    
    for (int i = 0; i<=NB_PODS; i++)
    {
        float prop_dt = (physicsTime - frameTime)/(frameTime - lastFrameTime);
        sf::Vector2f pos_sprite = podsSprites_[i].getPosition();
        podsSprites_[i].setPosition(pos_sprite + prop_dt * (pods_[i].pos_ - pos_sprite));

        if (pods_[i].laser_ )
        {
            sf::Vertex line[] =
            {
                sf::Vertex(sf::Vector2f(pods_[i].pos_)),
                sf::Vertex(sf::Vector2f(pods_[i].pos_ + sf::Vector2f(pods_[i].vel_.x *100, pods_[i].vel_.y *100)))
            };
        podsLasers.emplace_back(line);
        }
        
    }
}

void Game::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(backgroundSprite_, states);
    target.draw(finalCP_, states);

    for (const auto &cp : otherCPs_)
    {
        target.draw(cp, states);
    }

    for (const auto &podSprite : podsSprites_)
    {
        target.draw(podSprite, states);
    }
/* //affichage du laser, ne fonctionne actuellement pas
    for (int i = 0; i < NB_PODS; i++)
    {
        target.draw(podsLasers[i], 2, sf::Lines);
    }
    */
}
